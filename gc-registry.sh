#!/usr/bin/env bash
set -e
cd $(dirname $0)

REGISTRY_URL=${1:-http://127.0.0.1:5000}

docker run --rm \
    -v /tmp/manifest:/tmp/manifest \
    -v $PWD/docker-registry-clean.py:/docker-registry-clean.py \
    python:alpine /docker-registry-clean.py --registry-url=$REGISTRY_URL

docker exec registry registry garbage-collect --delete-untagged=true /etc/docker/registry/config.yml
