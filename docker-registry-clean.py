#!/usr/bin/env python3

import urllib.request
import urllib.parse
import argparse
import typing
import json
import logging
import pprint
import datetime


JSON_CONTNET_TYPE = 'application/json'
MANIFEST_CONTENT_TYPE = 'application/vnd.docker.distribution.manifest.v2+json'


class ImageTag:
    def __init__(self):
        self.repository = ''
        self.tag = ''
        self.create_time = ''
        self.digest = ''


def create_args_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser()
    parser.add_argument('--registry-url', required=True,
                        dest='registry_url', help="Docker registry url")
    parser.add_argument('--tag-expired-days', dest='tag_expired_days', default=30,
                        help='Delete image tag older than specified days except for latest')
    parser.add_argument('--manifest-list', dest='manifest_list',
                        help='list of registry manifest digest list', default='/tmp/manifest')
    return parser


def get_repositories(registry_url: str) -> typing.Sequence[str]:
    req = urllib.request.Request(
        urllib.parse.urljoin(registry_url, "/v2/_catalog"), headers={'Accpet': JSON_CONTNET_TYPE})
    logging.info("Getting registry repositories")
    with urllib.request.urlopen(req) as resp:
        return json.load(resp)['repositories']


def get_image_tags(registry_url: str, repositories: typing.Sequence[str]) -> typing.Sequence[ImageTag]:
    re: typing.Sequence[ImageTag] = []
    for repo in repositories:
        logging.info("Getting tag list of %s", repo)
        url = urllib.parse.urljoin(
            registry_url, "/v2/{0}/tags/list".format(repo))
        req = urllib.request.Request(
            url, headers={'Accpet': JSON_CONTNET_TYPE})
        with urllib.request.urlopen(req) as resp:
            jdata = json.load(resp)
            name = jdata['name']
            for tag in jdata['tags'] or []:
                info = ImageTag()
                info.repository = name
                info.tag = tag
                re.append(info)
    return re


def fill_tag_infos(registry_url: str, infos: typing.Sequence[ImageTag]):
    for info in infos:
        logging.info('Get info of %s:%s', info.repository, info.tag)
        url = urllib.parse.urljoin(
            registry_url, "/v2/{0}/manifests/{1}".format(info.repository, info.tag))
        req = urllib.request.Request(
            url, headers={'Accept': MANIFEST_CONTENT_TYPE})
        config_digest = ''
        with urllib.request.urlopen(req) as resp:
            jdata = json.load(resp)
            info.digest = resp.info()['docker-content-digest']
            config_digest = jdata['config']['digest']
        url = urllib.parse.urljoin(
            registry_url, "/v2/{0}/blobs/{1}".format(info.repository, config_digest))
        req = urllib.request.Request(
            url, headers={'Accept': MANIFEST_CONTENT_TYPE})
        with urllib.request.urlopen(req) as resp:
            jdata = json.load(resp)
            info.create_time = jdata['created']


def filter_to_remove_manifests(tag_expired_days: int, infos: typing.Sequence[ImageTag]) -> typing.Sequence[str]:
    now = datetime.datetime.now()
    re = []
    for info in infos:
        if info.tag == 'latest':
            continue
        create_time = datetime.datetime.strptime(
            info.create_time.split('.')[0], '%Y-%m-%dT%H:%M:%S')
        if create_time + datetime.timedelta(days=tag_expired_days) < now:
            re.append('{0} {1}'.format(info.repository, info.digest))
    return re


def delete_manifest(registry_url: str, repository: str, digest: str):
    logging.info('Delete manifest {0}:{1}'.format(repository, digest))
    url = urllib.parse.urljoin(
        registry_url, "/v2/{0}/manifests/{1}".format(repository, digest))
    req = urllib.request.Request(url, method='DELETE')
    with urllib.request.urlopen(req) as resp:
        assert resp.getcode() == 202


def main():
    logging.basicConfig(format='%(levelname)s:%(message)s',
                        level=logging.DEBUG)
    parser = create_args_parser()
    args = parser.parse_args()

    tag_expired_days: int = args.tag_expired_days

    registry_url = args.registry_url
    repositories = get_repositories(registry_url)
    image_tags = get_image_tags(registry_url, repositories)
    fill_tag_infos(registry_url, image_tags)

    manifests_to_remove = filter_to_remove_manifests(
        tag_expired_days, image_tags)

    for x in manifests_to_remove:
        parts = x.split(' ')
        delete_manifest(registry_url, parts[0], parts[1])


if __name__ == '__main__':
    main()
